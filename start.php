<?php
/*
Plugin Name: Multiple Category Select Widget (MCSW)
Plugin URI: http://www.anshuverma.com
Description: Displayes category on floating menu. You can change the location of the floating menu. 
Version: 1.0.0
Author: Anshu Verma
Author URI: http://www.anshuverma.com/
License: GNU General Public License v2.0
License URI: http://www.gnu.org/licenses/gpl-2.0.html


Copyright 2013  Anshu Verma (anshu@anshuverma.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
    
*/


  /**
   * For the header.
   */
   
function MCSW_header(){
wp_enqueue_script('jquery');
echo '<link rel="stylesheet" type="text/css" href="' .plugins_url('/style.css', __FILE__). '">';

if ($_POST) {
  $categoryids = $_POST['category'];
  if($categoryids)
    {
        foreach($categoryids as $categoryid)
        {
            $cat.=  $categoryid.',';
        }
        $cat = trim($cat, ',');
        $cat = 'cat=' . trim($cat, ',');
        query_posts( $cat );
    }
  }
}

  /**
   * For the Footer
   */

function MCSW_footer(){
  if(get_option("MCSW_Floating")==1)
  {
?>
<div id="MCSWfloating">
<form role="search" method="post" id="searchform" action="<?php bloginfo('url'); ?>/?search">
<?php 
        
        $categories=get_categories();
        foreach($categories as $category) {
          echo "<input type='checkbox' name='category[]' value='$category->cat_ID' /> ";
          echo $category->cat_name;
          echo '&nbsp;&nbsp;';
  }
  echo '&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" id="searchsubmit" value="Filter" />';
  echo '</form>';
  echo '</div>';
  }
}


function MCSW_admin() {
  if ($_POST) {
    $useroption = (isset($_POST['MCSW_float_include'])) ? 1 : 0;
    update_option("MCSW_Floating" ,$useroption);
  }
  $MCSW_Floating = get_option("MCSW_Floating");
  ?>
  <div class="wrap">
  <div id="icon-tools" class="icon32"></div><h2>MCSW Settings</h2><br />
<form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
  <input type="checkbox" name="MCSW_float_include" value="1" <?php if($MCSW_Floating==1) echo 'checked="checked"'; ?> /> Include Floating Footer Category Filter Menu () <br /><br />
  <input class='button-primary' type='submit' name='Save' value='<?php _e('Save Options'); ?>' id='submitbutton' />
</form>
</div>
<?php
}  

function MCSW_admin_actions() {
  add_option("MCSW_Floating", "0");
  add_options_page("MCSW", "MCSW Settings", 1, "MCSWSettings", "MCSW_admin");
}

/**
   * For the Widget
   */

class MCSW_widget extends WP_Widget {

  function __construct() {
  parent::__construct(
  'MCSW_widget', 
  __('MCSW Widget', 'MCSW_widget_domain'), 
  array( 'description' => __( 'MCSW Widget to add Category Filter Form', 'MCSW_widget_domain' ), ) 
  );
  }


  public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );
    echo $args['before_widget'];
    if ( ! empty( $title ) )
    echo $args['before_title'] . $title . $args['after_title'];
    ?>
    <form role="search" method="post" id="searchform" action="<?php bloginfo('url'); ?>/?search">
    <?php 
    $categories=get_categories();
    foreach($categories as $category) {
    echo "<input type='checkbox' name='category[]' value='$category->cat_ID' /> ";
    echo $category->cat_name;
    echo '<br />';
    }
    echo '<br /><input type="submit" id="searchsubmit" value="Filter" />';
    echo '</form>';
    echo $args['after_widget'];
  }
      

  public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
    $title = $instance[ 'title' ];
  }
  else {
    $title = __( 'New title', 'MCSW_widget_domain' );
  }

  ?>
  <p>
  <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
  <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
  </p>
  <?php 
  }

  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
  }
}

function MCSW_load_widget() {
  register_widget( 'MCSW_widget' );
}

add_action('widgets_init', 'MCSW_load_widget');
add_action('admin_menu', 'MCSW_admin_actions');
add_action('wp_head', MCSW_header, 1);
add_action('wp_footer', MCSW_footer, 1);

?>